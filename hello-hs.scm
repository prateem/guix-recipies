(define-module (hello-hs)
  #:use-module (guix build-system haskell)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (gnu packages haskell-check)
  ;; #:use-module (gnu packages tls)
  ;; #:use-module (gnu packages web)
  ;; #:use-module (gnu packages ssh)
  )

(define-public hello-hs
  (package
   (name "hello-hs")
   (version "0.1.0.0")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://gitlab.com/prateem/hello-hs")
       (commit "4285b068062237ccf6c8c06c953c964c5212cae3")))
     (sha256 (base32 "111ayhdl7y0rr6wv62rjg4wxs53yrhk8dm9l573c090kssd80fi1"))))
   (build-system haskell-build-system)
   (native-inputs `(("ghc-hspec" ,ghc-hspec)
		    ("ghc-quickcheck" ,ghc-quickcheck)))
   (synopsis "Basic haskell package")
   (description "This package has one executable that prints \"Hello, Haskell!\"")
   (home-page "https://gitlab.com/prateem/hello-hs")
   (license gpl3+)))
