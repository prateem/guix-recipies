{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "prateem";
  home.homeDirectory = "/home/prateem";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # services.emacs.package = pkgs.emacsGit;

  nixpkgs.overlays = [
    (import (builtins.fetchTarball {
      url = https://github.com/nix-community/emacs-overlay/archive/master.tar.gz;
    }))
  ];

  home.packages = with pkgs; [
    # dev tools
    git
    curl
    wget
    jq
    tree
    htop
    vim
    gnupg
    asciinema
    asciinema-agg

    # java
    jdk
    maven
    jdt-language-server

    # google cloud
    google-cloud-sdk

    # fonts
    fira
  ];

  programs.bash = {
    enable = true;
    profileExtra = ''
      # if [ -e $HOME/.nix-profile/etc/profile.d/nix.sh ]; then
      . $HOME/.nix-profile/etc/profile.d/nix.sh

      # if [ -e $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh ]; then
      # . $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh

      if [ -d $HOME/profile.d ]; then
        for i in $HOME/profile.d/*.sh; do
          if [ -r $i ]; then
            . $i
          fi
        done
        unset i
      fi
    '';
    bashrcExtra = ''
      export JAVA_HOME="$HOME/.nix-profile/lib/openjdk";
      export JAVAX_NET_SSL_TRUSTSTORE="/home/prateem/Programs/java/cacerts";
    '';
  };
  
  programs.git = {
    enable = true;
    userName = "Prateem Mandal";
    userEmail = "prateem@gmail.com";
  };
    
  programs.emacs = {
    enable = true;
    package = pkgs.emacsGit;
    extraPackages = epkgs: with epkgs; [
      cl-lib
      nix-mode
      magit
      exec-path-from-shell
      smex
      projectile
      helm
      helm-descbinds
      helm-swoop
      which-key
      which-key-posframe
      paredit
      flycheck
      flycheck-guile
      yasnippet
      yasnippet-snippets
      company
      treemacs
      # tramp
      # tramp-auto-auth
      # tramp-nspawn
      # tramp-term
      # tramp-hdfs
      # tramp-theme
      geiser
      keycast
      request
      esxml
      vterm
      yaml-mode
      pdf-tools
      # dante
      lsp-mode
      lsp-ui
      helm-lsp
      lsp-treemacs
      haskell-mode
      flycheck-haskell
      lsp-haskell
      company-cabal

      # Java related
      exec-path-from-shell
      projectile
      which-key
      lsp-java
      
      # color themes
      color-theme-sanityinc-solarized
      color-theme-sanityinc-tomorrow
    ];
    extraConfig = ''
      (require 'cl-lib)
      (require 'package)

      (setq standard-indent 2)

      ;; default is 800kb, measured in bytes
      (setf gc-cons-threshold (* 50 1000 1000))

      ;; profile emacs startup
      (add-hook 'emacs-startup-hook
	              (lambda ()
	                (message "Emacs loaded in %s seconds with %d garbage collections."
		              (emacs-init-time "%.2f")
		              gcs-done)))

      ;; silence compiler warnings
      (setf native-comp-async-report-warnings-errors nil)

      ;; set the right directory to store the native comp cache
      (add-to-list 'native-comp-eln-load-path (expand-file-name "eln-cache/" user-emacs-directory))

      ;; set the compilation level to 3
      (setf native-comp-speed 3)

      ;; set compiler options
      (setf native-comp-compiler-options '("-O3"))

      ;; load shell environment variables into emacs
      (exec-path-from-shell-initialize)

      ;; language environment
      (set-language-environment "UTF-8")
      (set-default-coding-systems 'utf-8)

      ;; paredit post config
      (dolist
        (m
          '(emacs-lisp-mode-hook
            eval-expression-minibuffer-setup-hook
            ielm-mode-hook
            lisp-mode-hook
            lisp-interaction-mode-hook
            scheme-mode-hook
            geiser-repl-mode-hook))
        (add-hook m #'enable-paredit-mode))
      ;; (bind-keys :map paredit-mode-map
      ;;   ("{" . paredit-open-curly)
      ;;   ("}" . paredit-close-curly))
      ;; (unless terminal-frame
      ;;   (bind-keys :map paredit-mode-map
      ;;     ("M-[" . paredit-wrap-square)
      ;;     ("M-{" . paredit-wrap-curly)))

      ;; helm config
      (helm-mode 1)
      (setf helm-buffers-fuzzy-matching t)
      ;; (bind-keys :map helm-command-map
      ;;   ("C-c h"   . helm-command-prefix)
      ;;   ("M-x"     . helm-M-x)
      ;;   ("C-x C-f" . helm-find-files)
      ;;   ("C-x b"   . helm-buffers-list)
      ;;   ("C-c b"   . helm-bookmarks)
      ;;   ("C-c f"   . helm-recentf)
      ;;   ("C-c g"   . helm-grep-do-git-grep))

      ;; orgmode open source window by vertically splitting on right
      (setf org-src-window-setup 'split-window-right)

      ;; maintain a list of most recent files opened
      (recentf-mode 1)
      (setf recentf-max-saved-items 32)

      ;; enable projectile
      (projectile-mode +1)

      ;; enable which-key
      (which-key-mode)

      ;; turn on yassnippets globally
      (yas-global-mode)

      ;; turn on flycheck globally
      (global-flycheck-mode)

      ;; turn on lsp-java for java files
      (add-hook 'java-mode-hook 'lsp)

      ;; support for sprint boot
      (require 'lsp-java-boot)
      (add-hook 'lsp-mode-hook #'lsp-lens-mode)
      (add-hook 'java-mode-hook #'lsp-java-boot-lens-mode)

      ;; set custom theme
      (when (display-graphic-p)
        (add-hook 'after-init-hook (lambda () (load-theme 'sanityinc-solarized-dark))))

      ;; set fonts
      (custom-set-faces
        '(default   ((t (:family "Fira Code" :foundry "CTDB" :slant normal :weight bold :height 143 :width normal))))
        '(mode-line ((t (:family "Fira Code" :foundry "CTDB" :slant normal :weight bold :height 143 :width normal)))))

      ;; disable splash screen
      (setf inhibit-splash-screen t)
      (setf inhibit-startup-screen t)
      (setf inhibit-startup-message t)

      ;; highlight closing parens
      (show-paren-mode 1)

      ;; modebar metrics
      (column-number-mode t)
      (setf display-time-date-and-day t)

      ;; cleanup the ui
      (menu-bar-mode -1)
      (scroll-bar-mode -1)
      (tool-bar-mode -1)
      (tooltip-mode -1)
      (fringe-mode 0)
      (setf inhibit-startup-message t)
    '';
  };
}
